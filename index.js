import * as React from 'react'
import ReactDOM from 'react-dom'
import App from 'src/app'

const root = document.getElementById('root')

ReactDOM.unstable_createRoot(root).render(<App />)
